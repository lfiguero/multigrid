import numpy as np
from scipy.sparse import coo_matrix

refM = np.array([[2, 1, 1], [1, 2, 1], [1, 1, 2]]) / 24.0
gradbaryrhs = np.array([[1, 0], [0, 1], [0, 0]])

def quadrature_rule_7_5():
    """Computes nodes and weights of a cuadrature rule for the unit triangle

    It consists of 7 points and weights and is exact for polynomials up to
    degree 5.
    """
    sq15 = np.sqrt(15.0)
    a = 1.0/3.0
    b = (9.0 + 2.0*sq15)/21.0
    c = (6.0 - sq15)/21.0
    d = (9.0 - 2.0*sq15)/21.0
    e = (6.0 + sq15)/21.0
    u = 0.1125
    v = (155.0 - sq15)/2400.0
    w = (155.0 + sq15)/2400.0
    points = np.array([[a, a], [b, c], [c, b], [c, c], [d, e], [e, d], [e, e]])
    weights = np.array([u, v, v, v, w, w, w])
    return (points, weights)

def quadrature_rule_1_1():
    """Computes nodes and weights of a cuadrature rule for the unit triangle

    It consists of 1 point and 1 weight and is exact for polynomials up to
    degree 1.
    """
    points = np.array([[1.0/3.0, 1.0/3.0]])
    weights = np.array([1.0/2.0])
    return (points, weights)


def localContributions(coords):
    """Computes local contributions to finite element matrices

    Parameters:
    -----------
    coords: 3x2 array with the coordinates of a triangle.
    """
    BT = np.array([
	[coords[1,0] - coords[0,0], coords[2,0] - coords[0,0]],
	[coords[1,1] - coords[0,1], coords[2,1] - coords[0,1]]
	])
    bT = coords[0]
    detBT = BT[0,0]*BT[1,1] - BT[0,1]*BT[1,0]
    localM = detBT * refM
    # The following three lines are equivalent to
    # baryDefMat = np.r_[coords.T, [[1.0, 1.0, 1.0]]]
    # but way faster
    baryDefMatList = coords.T.tolist()
    baryDefMatList.append([1.0, 1.0, 1.0])
    baryDefMat = np.array(baryDefMatList)
    aux = np.linalg.solve(baryDefMat, gradbaryrhs)
    localK = detBT/2.0 * aux.dot(aux.T)
    return (localM, localK)

def finiteElementMatrices(mesh):
    """Assembles finite element matrices

    This function takes as its argument an instance of the class
    meshManipulation.mesh.

    It returns the tuple (M, K), where M and K are, respectively, the mass and
    stiffness matrix of the continuous P1 finite element space based on the
    given mesh.
    """
    localtuples = [localContributions(coords) for coords in mesh.p[mesh.t]]
    localMs, localKs = zip(*localtuples)
    rowidx = [np.tile(vtx, (3,1)) for vtx in mesh.t]
    colidx = [r.T for r in rowidx]
    # Now I flatten everything
    localMs = np.r_[localMs].flat
    localKs = np.r_[localKs].flat
    rowidx = np.r_[rowidx].flat
    colidx = np.r_[colidx].flat
    M = coo_matrix((localMs, (rowidx, colidx)), shape=(len(mesh.p), len(mesh.p))).tocsr()
    K = coo_matrix((localKs, (rowidx, colidx)), shape=(len(mesh.p), len(mesh.p))).tocsr()
    return (M,K)

def loadVector(mesh, f):
    """Assembles load vector

    Parameters:
    mesh: instance of the class meshManipulation.mesh.
    f: function that given a Numpu float array of shape (M1, ..., M_k, 2)
    returns a Numpy float array of shape (M_1, ..., M_k).
    """
    pts, wts = quadrature_rule_1_1() # Good enough for this
    pts_bary = np.c_[1.0 - (pts[:,0] + pts[:,1]), pts[:,0], pts[:,1]]
    out = np.zeros(len(mesh.p))
    for vtx in mesh.t:
	coords = mesh.p[vtx]
	detBT = (coords[1,0] - coords[0,0])*(coords[2,1] - coords[0,1]) -\
		(coords[2,0] - coords[0,0])*(coords[1,1] - coords[0,1])
	transf_pts = pts_bary.dot(coords)
	feval = f(transf_pts)
	f_times_basis_functions = feval[:,np.newaxis]*pts_bary
	out[vtx] = out[vtx] + detBT * wts.dot(f_times_basis_functions)
    return out
