import numpy as np # Basic operations with floating point arrays
from meshpy.triangle import MeshInfo, build
from scipy.sparse.linalg import eigsh
import time
from meshManipulation import mesh, refinedMesh
from assembly import finiteElementMatrices, loadVector
from errorMeasuring import L2_distance
import matplotlib.pyplot as plt # Plotting

tm = [time.time()]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Definition of a right hand side function #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
def f(x):
    xx = x[..., 0]
    yy = x[..., 1]
    return -24.0*(-1.0 + xx)*xx*np.cos(12.0*xx*yy) - \
            24.0*(-1.0 + yy)*yy*np.cos(12.0*xx*yy) + \
            144.0*(-1.0 + xx)*xx**2*(-1.0 + yy)*np.sin(12.0*xx*yy) + \
            144.0*(-1.0 + xx)*(-1.0 + yy)*yy**2*np.sin(12.0*xx*yy)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Solution of $-\Delta u = f$ with zero Dirichlet boundary conditions #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
def sol(x):
    xx = x[..., 0]
    yy = x[..., 1]
    return np.sin(12.0*xx*yy)*(xx-1.0)*(yy-1.0)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Construction of initial mesh #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
mesh_info = MeshInfo()
mesh_info.set_points([
    (0,0), (1.0,0), (1.0,1.0), (0,1.0),
    ])
mesh_info.set_facets([
    [0,1],
    [1,2],
    [2,3],
    [3,0],
    ])
initialMesh = build(mesh_info, max_volume=0.1)

p = np.array(initialMesh.points)
t = np.array(initialMesh.elements)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Construction of nested meshes and operators #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
levels = 5
meshes = [mesh(p,t)]
for k in range(1, levels):
    meshes.append(refinedMesh(meshes[-1]))
# Assembly
M, K = finiteElementMatrices(meshes[-1])
l = loadVector(meshes[-1], f)
# Identification of non-Dirichlet node indices for each mesh
nonDirPts = []
for m in meshes:
    DirPts = np.zeros(len(m.p), dtype=bool)
    DirPts[m.e[m.eDir]] = True
    # From all node indices (i.e., np.arange(len(m.p))) we choose those which
    # are not ends of Dirichlet edges
    nonDirPts.append(np.arange(len(m.p))[np.logical_not(DirPts)])
# For k in range(1, levels) we restrict meshes[k].I in order to take into
# account account the zero Dirichlet boundary condition on the two finite
# element spaces connected by meshes[k].I
extensions = []
for k in range(1, levels):
        extensions.append(meshes[k].I[nonDirPts[k]][:,nonDirPts[k-1]])
        #                            \____________/\________________/
        #                                  |               |
        #                /-----------------                |
        # Non-Dirichlet nodes row selection    Non-Dirichlet nodes column selection
        #
# Matrix form of differential operators; we use the output of the only
# conventionally assembled level (the highest one) and then use the extension
# operators and their transposes (the latter are projections)
A = [None]*levels
A[-1] = K[nonDirPts[-1]][:,nonDirPts[-1]]
for k in reversed(range(levels-1)):
    A[k] = extensions[k].T.dot(A[k+1].dot(extensions[k]))
# Each full (i.e., non-restricted to non-Dirichlet nodes) load vector up to the
# second-to-last level is the projection of the load vector of the next level;
# we build those full load vectors and only then we restrict each of them to
# the entries corresponding to non-Dirichlet nodes
rhs = [None]*levels
rhs[-1] = l
for k in reversed(range(levels-1)):
    rhs[k] = meshes[k+1].I.T.dot(rhs[k+1])
for k in range(levels):
    rhs[k] = rhs[k][nonDirPts[k]]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Computation of appropriately growing spectral radii #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Now I solve the generalized eigenvalue problem on the coarsest mesh which is
# small, so I use a very crude direct method for this
ew = np.linalg.eigvals(
    A[0].todense()/meshes[0].h**2
    )
# So, an appropriate Lambda for the first mesh is the maximum of ew; later,
# assuming that the true Lambda[k] behaves like C*meshes[k].h**(-2) for some C,
# then Lambda[k] behaves like Lambda[0]*(meshes[k].h/meshes[0].h)**(-2)
Lambda = max(ew) *\
        np.array([ (meshes[k].h/meshes[0].h)**(-2) for k in range(levels)])

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Definition of multigrid iterations #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
m = 3 # Number of pre- and postsmoothings
p = 1 # Number of error correction steps (V-, W-cycle if p=1, 2, resp.)
r = 50 # Number of times the level iterations are called directly

# Level iterations
def MG(k, z, g):
    if k == 0:
        return np.linalg.solve(A[k].todense(), g)
    else:
        # Presmoothing
        for l in range(m):
            z = z + 1.0/Lambda[k] * (g - A[k].dot(z))
        # Error correction
        q = np.zeros(np.count_nonzero(nonDirPts[k-1]))
        bar_g = extensions[k-1].T.dot(g - A[k].dot(z))
        for i in range(p):
            q = MG(k-1, q, bar_g)
        z = z + extensions[k-1].dot(q)
        # Postsmoothing
        for l in range(m):
            z = z + 1.0/Lambda[k] * (g - A[k].dot(z))
        return z

# Full multigrid algorithm
tm.append(time.time())
u = np.linalg.solve(A[0].todense(), rhs[0])
for k in range(1, levels):
    u = extensions[k-1].dot(u)
    for l in range(r):
        u = MG(k, u, rhs[k])
tm.append(time.time()); print "Multigrid time without set-up (s)", tm[-1] - tm[-2]

# We complete u with the entries we know are zero because of the Dirichlet
# boundary conditions
full_u = np.zeros(len(meshes[-1].p))
full_u[nonDirPts[-1]] = u

print "L^2 error:", L2_distance(meshes[-1], full_u, sol)

tm.append(time.time()); print "Total time (s):", tm[-1] - tm[0]
