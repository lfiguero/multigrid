import numpy as np
from assembly import quadrature_rule_7_5

def L2_distance(mesh, uh, u):
    """Computes L^2 distance between a continuous P1 function and a function

    Parameters:
    mesh: Member of the meshManipulation.mesh class.
    uh: Member of continuous P1 with respect to the given mesh representad as a
    Numpy float array of size len(mesh.p).
    u: Function which takes a Numpy float array of shape (M_1, ..., M_k, 2) and
        returns a Numpy float array of shape (M1, ..., M_k).
    """
    import matplotlib.pyplot as plt
    out = 0.0
    pts, wts = quadrature_rule_7_5()
    pts_bary = np.c_[1.0 - (pts[:,0] + pts[:,1]), pts[:,0], pts[:,1]]
    for i, vtx in enumerate(mesh.t):
	coords = mesh.p[vtx]
	detBT = (coords[1,0] - coords[0,0])*(coords[2,1] - coords[0,1]) -\
		(coords[2,0] - coords[0,0])*(coords[1,1] - coords[0,1])
	transf_pts = pts_bary.dot(coords)
	ueval = u(transf_pts)
	uheval = pts_bary.dot(uh[vtx])
	out = out + detBT * wts.dot((ueval-uheval)**2)
    return np.sqrt(out)
