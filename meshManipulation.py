import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from scipy.sparse import eye, coo_matrix, bmat

class mesh:
    """Mesh container class

    The initialization call is mesh(p, t), where
    p: Two-column float array with node coordinates
    t: Three-column int array with the indices of the vertices of each element

    Attributes:
    p: Same as above
    t: Same as above
    e: Two-column int array with the indices of vertices of each edge
    t2e: Three column int array with the indices of the edges of each element;
        t2e[i,j] is the edge of the triangle i opposite the vertex t[i,j]
    eDir: Boolean array describing whether edges belong to the Dirichlet part
        of the boundary
    h: Mesh size (maximal edge length)
    """
    def __init__(self, p, t):
	self.p = p
	self.t = t
	self.e, self.t2e, self.eDir = self.build_e(t)
	self.h = np.linalg.norm(self.p[self.e[:,1]] - self.p[self.e[:,0]], axis=1).max()
    def build_e(self, t):
	"""Constructs a list of edges and associated arrays"""
	e = { frozenset((tri[(i+1)%3], tri[(i+2)%3])) for tri in t for i in range(3)}
	e = list(e)
	t2e = np.zeros_like(t)
	eDir = np.zeros(len(e), dtype=bool)
	for (ind, tri) in enumerate(t):
	    for i in xrange(3):
		local_e = frozenset((tri[(i+1)%3], tri[(i+2)%3]))
		global_e = e.index(local_e)
		eDir[global_e] = not(eDir[global_e])
		t2e[ind, i] = global_e
	e = np.array([list(ee) for ee in e])
	return (e, t2e, eDir)
    def draw(self):
        """Plots the mesh

        After calling this method matplotlib.pyplot.show should be invoked
        (otherwise no plots will show)
        """
	import matplotlib.pyplot as plt
        plt.figure()
        for i, e in enumerate(self.e):
            coords = self.p[e]
            if self.eDir[i]:
                linestyle = 'b-'
                lw = 3
            else:
                linestyle = 'k-'
                lw = 1
            plt.plot(coords[:,0], coords[:,1], linestyle, linewidth=lw)
            md = coords.mean(axis=0)
            plt.text(md[0], md[1], '%d' % i, horizontalalignment='center',
                    verticalalignment='center',
                    backgroundcolor=(1.0, 0.75, 0.75, 0.5))
        for i, vc in enumerate(self.p):
            plt.text(vc[0], vc[1], '%d' % i, horizontalalignment='center',
                    verticalalignment='center',
                    backgroundcolor=(1.0, 1.0, 0.5, 0.5))
        for i, t in enumerate(self.t):
            coords = self.p[t]
            bc = coords.mean(axis=0)
            txt = 'v: ' + ('%s' % t) + '\n' + 'e: ' + ('%s' % self.t2e[i])
            plt.text(bc[0], bc[1], txt, horizontalalignment='center',
                    verticalalignment='center')
    def sanityTest(self):
        """Tests whether the attributes t and t2e are consistent
        """
        successFlag = True
        for i, vtx in enumerate(self.t):
            edgidx = self.t2e[i]
            for j in xrange(3):
                successFlag = successFlag and \
                        set(self.e[edgidx[j]]) == set([vtx[(j+1)%3], vtx[(j+2)%3]])
        return successFlag
    def plotP1(self, u):
	"""Plots a member of continuous P1 given by its nodal values
	
	After calling this method matplotlib.pyplot.show should be invoked.
	"""
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	ax.plot_trisurf(self.p[:,0], self.p[:,1], u, triangles=self.t, cmap=plt.cm.hsv)


class refinedMesh(mesh):
    """Uniform mesh refinement class
    
    The initialization call is refinedMesh(mesh), where:
    mesh: instance of class mesh

    Attributes:
    p, t, e, t2e, eDir, h: Same as mesh
    I: Extension matrix; i.e., matrix which when multiplied by a vector
	representing a continuous P1 function with respect to the parent mesh
	returns the vector representing the same function with respect with the
	refined mesh
    """
    def __init__(self, coarseMesh):
	# Construction of new node collection
	newp = (coarseMesh.p[coarseMesh.e[:,0]] +
		coarseMesh.p[coarseMesh.e[:,1]]) / 2.0
	self.p = np.r_[coarseMesh.p, newp]
	# Bisection of old edges
	newpIndices = len(coarseMesh.p) + np.arange(len(coarseMesh.e))
	first_halves = np.c_[coarseMesh.e[:,0], newpIndices]
	second_halves = np.c_[newpIndices, coarseMesh.e[:,1]]
	# Creation of edges in the interior of old elements and new elements
	newt = np.zeros((4*len(coarseMesh.t),3), dtype=coarseMesh.t.dtype)
	newt2e = np.zeros((4*len(coarseMesh.t),3), dtype=coarseMesh.t2e.dtype)
	inneredges = np.zeros((3*len(coarseMesh.t),2), dtype=coarseMesh.e.dtype)
	for (ind, tri) in enumerate(coarseMesh.t):
	    # Global indices of edges (resp. vertices) of old triangle
	    GIEOT = coarseMesh.t2e[ind]
            GIVOT = coarseMesh.t[ind]
	    for j in xrange(3):
		inneredges[3*ind+j,0] = len(coarseMesh.p) + GIEOT[(j+1)%3]
		inneredges[3*ind+j,1] = len(coarseMesh.p) + GIEOT[(j+2)%3]
                newt[4*ind+j] = [GIVOT[j], len(coarseMesh.p) + GIEOT[(j+2)%3],
                        len(coarseMesh.p) + GIEOT[(j+1)%3]]
                t2eaux = [2*len(coarseMesh.e) + 3*ind+j, 0, 0]
                for i in (1, 2):
                    if coarseMesh.e[GIEOT[(j+i)%3]][0] == GIVOT[j]:
                        t2eaux[i] = GIEOT[(j+i)%3]
                    elif coarseMesh.e[GIEOT[(j+i)%3]][1] == GIVOT[j]:
                        t2eaux[i] = len(coarseMesh.e) + GIEOT[(j+i)%3]
                    else:
                        print "Error in construction of t2e array"
                newt2e[4*ind+j] = t2eaux
            # Fill details of the new triangle in the center of the old
            # triangle
            newt[4*ind+3] = len(coarseMesh.p) + GIEOT
            newt2e[4*ind+3] = 2*len(coarseMesh.e) + 3*ind + np.arange(3)
        self.e = np.r_[first_halves, second_halves, inneredges]
        self.eDir = np.r_[coarseMesh.eDir, coarseMesh.eDir, np.zeros(3*len(coarseMesh.t), dtype=bool)]
        self.t = newt
        self.t2e = newt2e
	self.h = coarseMesh.h/2.0
	self.I = self.build_I(len(coarseMesh.p), coarseMesh.e)
    def build_I(self, nOldNodes, oldEdges):
	"""Construction of extension matrix"""
	I1 = eye(nOldNodes)
	rowcolval = [([i, i],e.tolist(),[0.5, 0.5]) for i, e in enumerate(oldEdges)]
	rowidx, colidx, val = zip(*rowcolval)
	rowidx = np.r_[rowidx]
	colidx = np.r_[colidx]
	val = np.r_[val]
	I2 = coo_matrix((val, (rowidx, colidx)), shape=(len(oldEdges), nOldNodes))
	return bmat([[I1], [I2]]).tocsr()
