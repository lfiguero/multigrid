Implementation of the multigrid algorithm described in chapter 6 of Brenner & Scott: The Mathematical Theory of Finite Element Methods, 3rd edition, Springer (2008)
