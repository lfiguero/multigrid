import numpy as np # Basic operations with floating point arrays
from scipy.sparse.linalg import eigsh
from meshpy.triangle import MeshInfo, build
from meshManipulation import mesh, refinedMesh
from assembly import finiteElementMatrices
import matplotlib.pyplot as plt # Plotting

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Construction of initial mesh #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
mesh_info = MeshInfo()
mesh_info.set_points([
    (0,0), (1.0,0), (1.0,1.0), (0,1.0),
    ])
mesh_info.set_facets([
    [0,1],
    [1,2],
    [2,3],
    [3,0],
    ])
initialMesh = build(mesh_info, max_volume=0.01)

p = np.array(initialMesh.points)
t = np.array(initialMesh.elements)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Construction of a mesh hierarchy #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
m0 = mesh(p,t)
m1 = refinedMesh(m0)
M, K = finiteElementMatrices(m1)
ew, ev = eigsh(K, k=9, M=M, which='SM')
print ew/np.pi**2
print ev.shape
for i in xrange(ev.shape[1]):
    m1.plotP1(ev[:,i])
    plt.title("Eigenvalue:" + ('%f' % (ew[i]/np.pi**2)) + "*pi^2")
plt.show()
