import numpy as np # Basic operations with floating point arrays
from meshpy.triangle import MeshInfo, build
from scipy.sparse.linalg import spsolve
from meshManipulation import mesh, refinedMesh
from assembly import finiteElementMatrices, loadVector
from errorMeasuring import L2_distance
import matplotlib.pyplot as plt # Plotting

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Definition of a right hand side function #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
def f(x):
    localx = x[..., 0]
    localy = x[..., 1]
    return -12.0 + 12.0*localx + 3.0*localx**2 - 2.0*localx**3 + 12.0*localy + 3.0*localy**2 - 2.0*localy**3

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Exact solution to -\Delta u + u = f with zero Neumann boundary conditions #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
def sol(x):
    localx = x[..., 0]
    localy = x[..., 1]
    return (3-2.0*localx)*localx**2 + (3.0-2.0*localy)*localy**2

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Construction of initial mesh #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
mesh_info = MeshInfo()
mesh_info.set_points([
    (0,0), (1.0,0), (1.0,1.0), (0,1.0),
    ])
mesh_info.set_facets([
    [0,1],
    [1,2],
    [2,3],
    [3,0],
    ])
initialMesh = build(mesh_info, max_volume=0.2)

p = np.array(initialMesh.points)
t = np.array(initialMesh.elements)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Construction of a mesh hierarchy #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
m = [mesh(p,t)]
for k in range(5):
    m.append(refinedMesh(m[-1]))
h = np.array([mesh.h for mesh in m])
err = []
for mesh in m:
    M, K = finiteElementMatrices(mesh)
    l = loadVector(mesh, f)
    uh = spsolve(K+M, l)
    err.append(L2_distance(mesh, uh, sol))
err = np.array(err)
empirical_convergence_rate = np.log(err[1:]/err[:-1])/np.log(h[1:]/h[:-1])
# For a regular enough right-hand side and a convex domain we expect a
# convergence rate of 2.0
print empirical_convergence_rate
